#!/usr/bin/python

import sys
import os
import json
import shutil

pybower_path = os.path.dirname(os.path.realpath(__file__))
bower_path = os.path.join(pybower_path, "bower_components")
current_working_path = os.getcwd()

def clone_path_file():

	path_file = os.path.join(pybower_path, "path.json")

	try:
		shutil.copy(path_file, current_working_path)
		print "Copied: " + path_file
	except IOError, e:
		print e

def list_assets():

	lists = os.listdir(bower_path)
	for l in lists:
		print l

def load_paths():

	path_file = os.path.join(current_working_path, "path.json")
	if os.path.isfile(path_file):	
		path_data = open(path_file)

		paths = json.load(path_data)
		path_data.close()

		return paths
	else:
		print "You must use `pybower init` to clone for a local copy of path.json before using pybower"
		sys.exit()
	

def load_assets():

	asset_file = os.path.join(pybower_path, "asset.json")
	if os.path.isfile(asset_file):	
		asset_data = open(asset_file)

		assets = json.load(asset_data)
		asset_data.close()

		return assets
	else:
		print "The asset.json is probably missing, pybower cannot continue"
		sys.exit()

def copy_asset_file(asset_name):

	try:
		asset = assets[asset_name]
	except Exception, e:
		print "Invalid library's name: " + asset_name
		return

	print "\nlibrary: *** " + asset_name + " ***"
	print "=============================="

	for target in asset:

		dest_path = paths[target["dest"]]
		src_path = os.path.join(bower_path, target["src"]["path"])
		files = target["src"]["files"]

		if not os.path.exists(dest_path):
			os.makedirs(dest_path)

		for f in files:
			try:
				shutil.copy(os.path.join(src_path, f), dest_path)
				print "Copied: " + f + " => " + dest_path
			except IOError, e:
				print e

if __name__ == '__main__':

	if len(sys.argv) == 1:
		print "You did not specify any commands"
		sys.exit()

	cmd = sys.argv[1]

	if cmd == "init":
		clone_path_file()
	elif cmd == "list":
		list_assets()
	elif cmd == "install":
		if len(sys.argv) <= 2:
			print "You did not specify any libraries to install"
			sys.exit()

		paths = load_paths()
		assets = load_assets()
		for arg in sys.argv[2:]:
			copy_asset_file(arg)
	else:
		print "The command you specified was invalid, please try again"
