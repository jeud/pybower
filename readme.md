## pybower

This python ecosystem will copy all of each library's files from dedicated *bower's folder* according to the *asset.json* to the sub's folder (specified in *path.json*) of current working directory.

### 1. bower

#### Install bower

	npm install bower -g

#### Install bower components

	bower install jquery
	bower install anuglar

#### Maintain up-to-date bower's components

	bower update

### 2. Setup pybower

*pybower.py* should be setup using OS's alias, and must be installed at the same path as dedicated bower's folder

#### Windows

	pybower=python "D:\Users\Admin\Documents\home_www\pybower\pybower.py" $*

#### OSX

	alias pybower="python ~/home_www/pybower/pybower.py"

### 3. asset.json

*asset.json* must be located at the same path as *pybower.py*

	{
		"jquery": [
			{
				"src": {
					"path": "jquery/dist",
					"files": [
						"jquery.min.js",
						"jquery.min.map"
					]
				},
				"dest": "js"
			}
		],

		"bootstrap": [
			{
				"src": {
					"path": "bootstrap/dist/js",
					"files": [
						"bootstrap.min.js"
					]
				},
				"dest": "js"
			},
			{
				"src": {
					"path": "bootstrap/dist/css",
					"files": [
						"bootstrap.min.css",
						"bootstrap.css.map"
					]
				},
				"dest": "css"
			},
			{
				"src": {
					"path": "bootstrap/dist/fonts",
					"files": [
						"glyphicons-halflings-regular.eot",
						"glyphicons-halflings-regular.svg",
						"glyphicons-halflings-regular.ttf",
						"glyphicons-halflings-regular.woff"
					]
				},
				"dest": "fonts"
			}
		]
	}

### 4. path.json

The original *path.json* must be located at the same path as *pybower.py*

	{
		"css": "public/css",
		"js": "public/js",
		"fonts": "public/fonts"
	}

### 5. Usage

#### pybower with *init* command

Run `pybower init` from your working directory (root of your project) to copy the origin *path.json* to the working directory, so you can edit the destination path as you like

	pybower init

#### pybower with *list* command

Run `pybower list` to list all available bower libraries

	pybower list

#### pybower with install command and *library's names*

Run `pybower install lib1 lib2 libn` from your working directory to copy each of all library's files from dedicated *bower's folder* to the destination path specified on your local *path.json*

	pybower install jquery
	pybower install bootstrap angular requirejs